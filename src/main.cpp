#include <Arduino.h>
#include <BLEPeripheral.h>
#include "EEPROM.h"
#include "macro.h"

/*********** cau truc bien char ************/
typedef struct{
    bool flagNewDataUserSet = false;
    uint8_t dataChar[LEN_VALUE_CHAR];
}Data_t;

/********** bien luu data phone number ***********/
Data_t characticUser[NUM_CHARACTIC];

/********** note **********
 * 0-4 : user service save phone
 * 5-9 : user service save setting
 * 
 */

BLEPeripheral demo = BLEPeripheral();

BLEService phoneNumberService = BLEService("19b10000e8f2537e4f6cd104768a1214");
BLEFixedLengthCharacteristic phoneNumberCharacteristic1 = BLEFixedLengthCharacteristic("0001", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic phoneNumberCharacteristic2 = BLEFixedLengthCharacteristic("0002", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic phoneNumberCharacteristic3 = BLEFixedLengthCharacteristic("0003", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic phoneNumberCharacteristic4 = BLEFixedLengthCharacteristic("0004", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic phoneNumberCharacteristic5 = BLEFixedLengthCharacteristic("0005", BLERead|BLEWrite,LEN_VALUE_CHAR);

BLEService settingService = BLEService("19b10000e8f2537e4f6cd104768a1215");
BLEFixedLengthCharacteristic settingCharacteristic1 = BLEFixedLengthCharacteristic("0001", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic settingCharacteristic2 = BLEFixedLengthCharacteristic("0002", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic settingCharacteristic3 = BLEFixedLengthCharacteristic("0003", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic settingCharacteristic4 = BLEFixedLengthCharacteristic("0004", BLERead|BLEWrite,LEN_VALUE_CHAR);
BLEFixedLengthCharacteristic settingCharacteristic5 = BLEFixedLengthCharacteristic("0005", BLERead|BLEWrite,LEN_VALUE_CHAR);

/*********** khoi tao ham ************/
void bleHandleConnected(BLECentral& central);
void bleHandleDisconnect(BLECentral& central);
void loadAndSaveDatatoEEPROM(uint8_t *value,int addBegin,int len,Action_eeprom_t action);
void clearEeprom();
void saveData();
bool checkSaveData();

uint16_t ADD_FLASH_ARR[NUM_CHARACTIC] = {ADD_VALUE_SER_1_CHAR_1,ADD_VALUE_SER_1_CHAR_2,ADD_VALUE_SER_1_CHAR_3,ADD_VALUE_SER_1_CHAR_4,ADD_VALUE_SER_1_CHAR_5,
                                         ADD_VALUE_SER_2_CHAR_1,ADD_VALUE_SER_2_CHAR_2,ADD_VALUE_SER_2_CHAR_3,ADD_VALUE_SER_2_CHAR_4,ADD_VALUE_SER_2_CHAR_5};

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    
    /*********** set ting ble ***********/
    demo.setLocalName("Demo_vuhoang");
    demo.setAdvertisedServiceUuid(phoneNumberService.uuid());
    demo.addAttribute(phoneNumberService);
    demo.addAttribute(phoneNumberCharacteristic1);
    demo.addAttribute(phoneNumberCharacteristic2);
    demo.addAttribute(phoneNumberCharacteristic3);
    demo.addAttribute(phoneNumberCharacteristic4);
    demo.addAttribute(phoneNumberCharacteristic5);

    demo.setAdvertisedServiceUuid(settingService.uuid());
    demo.addAttribute(settingService);
    demo.addAttribute(settingCharacteristic1);
    demo.addAttribute(settingCharacteristic2);
    demo.addAttribute(settingCharacteristic3);
    demo.addAttribute(settingCharacteristic4);
    demo.addAttribute(settingCharacteristic5);
    demo.setConnectionInterval(0x0006,0x0c80);

    demo.setEventHandler(BLEConnected,bleHandleConnected);
    demo.setEventHandler(BLEDisconnected,bleHandleDisconnect);
    
    /************ set data buff ************/
    /*********** service 1 **************/
    // flashInit();
    // clearEeprom();
    loadAndSaveDatatoEEPROM(characticUser[0].dataChar,ADD_VALUE_SER_1_CHAR_1,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[1].dataChar,ADD_VALUE_SER_1_CHAR_2,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[2].dataChar,ADD_VALUE_SER_1_CHAR_3,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[3].dataChar,ADD_VALUE_SER_1_CHAR_4,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[4].dataChar,ADD_VALUE_SER_1_CHAR_5,LEN_VALUE_CHAR,LOAD_EEPROM);

    /*********** service 2 *************/
    loadAndSaveDatatoEEPROM(characticUser[5].dataChar,ADD_VALUE_SER_2_CHAR_1,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[6].dataChar,ADD_VALUE_SER_2_CHAR_2,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[7].dataChar,ADD_VALUE_SER_2_CHAR_3,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[8].dataChar,ADD_VALUE_SER_2_CHAR_4,LEN_VALUE_CHAR,LOAD_EEPROM);
    loadAndSaveDatatoEEPROM(characticUser[9].dataChar,ADD_VALUE_SER_2_CHAR_5,LEN_VALUE_CHAR,LOAD_EEPROM);

    phoneNumberCharacteristic1.setValue(characticUser[0].dataChar,LEN_VALUE_CHAR);
    phoneNumberCharacteristic2.setValue(characticUser[1].dataChar,LEN_VALUE_CHAR);
    phoneNumberCharacteristic3.setValue(characticUser[2].dataChar,LEN_VALUE_CHAR);
    phoneNumberCharacteristic4.setValue(characticUser[3].dataChar,LEN_VALUE_CHAR);
    phoneNumberCharacteristic5.setValue(characticUser[4].dataChar,LEN_VALUE_CHAR);

    /********** service 2 *************/
    settingCharacteristic1.setValue(characticUser[5].dataChar,LEN_VALUE_CHAR);
    settingCharacteristic2.setValue(characticUser[6].dataChar,LEN_VALUE_CHAR);
    settingCharacteristic3.setValue(characticUser[7].dataChar,LEN_VALUE_CHAR);
    settingCharacteristic4.setValue(characticUser[8].dataChar,LEN_VALUE_CHAR);
    settingCharacteristic5.setValue(characticUser[9].dataChar,LEN_VALUE_CHAR);

    demo.begin();

}

void loop()
{
    BLECentral centralDemo = demo.central();
    if(centralDemo)
    {
        while(centralDemo.connected())
        {
          // char phone number
            if(phoneNumberCharacteristic1.written())
            {   
                memcpy(characticUser[0].dataChar,phoneNumberCharacteristic1.value(),LEN_VALUE_CHAR);
                characticUser[0].flagNewDataUserSet = true;
            } 
            if(phoneNumberCharacteristic2.written())
            {
                memcpy(characticUser[1].dataChar,phoneNumberCharacteristic2.value(),LEN_VALUE_CHAR);
                characticUser[1].flagNewDataUserSet = true;
            }    
            if(phoneNumberCharacteristic3.written())
            {
                memcpy(characticUser[2].dataChar,phoneNumberCharacteristic3.value(),LEN_VALUE_CHAR);
                characticUser[2].flagNewDataUserSet = true;
            }    
            if(phoneNumberCharacteristic4.written())
            {
                memcpy(characticUser[3].dataChar,phoneNumberCharacteristic4.value(),LEN_VALUE_CHAR);
                characticUser[3].flagNewDataUserSet = true;
            }    
            if(phoneNumberCharacteristic5.written())
            {
                memcpy(characticUser[4].dataChar,phoneNumberCharacteristic5.value(),LEN_VALUE_CHAR);
                characticUser[4].flagNewDataUserSet = true;
            }    

          // char setting  
            if(settingCharacteristic1.written())
            {
                memcpy(characticUser[5].dataChar,settingCharacteristic1.value(),LEN_VALUE_CHAR);
                characticUser[5].flagNewDataUserSet = true;
            }       
            if(settingCharacteristic2.written())
            {
                memcpy(characticUser[6].dataChar,settingCharacteristic2.value(),LEN_VALUE_CHAR);
                characticUser[6].flagNewDataUserSet = true;
            }      
            if(settingCharacteristic3.written())
            {
                memcpy(characticUser[7].dataChar,settingCharacteristic3.value(),LEN_VALUE_CHAR);
                characticUser[7].flagNewDataUserSet = true;
            }      
            if(settingCharacteristic4.written())
            {
                memcpy(characticUser[8].dataChar,settingCharacteristic4.value(),LEN_VALUE_CHAR);
                characticUser[8].flagNewDataUserSet = true;
            }      
            if(settingCharacteristic5.written())
            {
                memcpy(characticUser[9].dataChar,settingCharacteristic5.value(),LEN_VALUE_CHAR);
                characticUser[9].flagNewDataUserSet = true;
            } 
        }
    }
    if(checkSaveData())
    {
        demo.end();
        saveData();
        demo.begin();
    }
}


void bleHandleConnected(BLECentral& central)
{
    return;
}

void bleHandleDisconnect(BLECentral& central)
{
    return;
}

bool checkSaveData()
{
    for(uint8_t i=0;i<NUM_CHARACTIC;i++)
    {
        if(characticUser[i].flagNewDataUserSet) return true;
    }
    return false;
}

void saveData()
{
    for(uint8_t i=0;i<NUM_CHARACTIC;i++)
    {
        if(characticUser[i].flagNewDataUserSet == true)
        {
            loadAndSaveDatatoEEPROM(characticUser[i].dataChar,ADD_FLASH_ARR[i],LEN_VALUE_CHAR,SAVE_EEPROM);
            characticUser[i].flagNewDataUserSet = false;
        }
    }
}

void loadAndSaveDatatoEEPROM(uint8_t *value,int addBegin,int len,Action_eeprom_t action)
{
    for(int index=addBegin;index<(addBegin + len);index++)
    {   
        if(action == SAVE_EEPROM)
        {   
            EEPROM.write(index,value[index - addBegin]);
        }
        else if(action == LOAD_EEPROM)
        {
            value[index - addBegin] = EEPROM.read(index);
        }
    }
}


void clearEeprom()
{
    for(int i=0;i<(ADD_VALUE_SER_2_CHAR_5 + LEN_VALUE_CHAR);i++)
    {
        EEPROM.write(i,0xFF);
    }
}